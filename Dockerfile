FROM ypereirareis/docker-satis

LABEL maintaner="Michał Mleczko <michal@mleczko.waw.pl>"

ADD config.php /app/config.php
ADD config.json /app/config.json

#RUN wget ssh_priv_key.tar.gz /var/tmp/id